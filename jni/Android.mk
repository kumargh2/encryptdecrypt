LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
SYSROOT="/home/kumar/android/android-ndk-r9/platforms/android-18/arch-arm"
OPENSSL_HEADERS="../include"
LOCAL_C_INCLUDES := ${OPENSSL_HEADERS}
ANDROID_HOME := ${ANDROID_SDK_HOME}
LOCAL_LDLIBS += -llog -L. -lcrypto
LOCAL_MODULE    := openssl-android
LOCAL_SRC_FILES := openssl-android.cpp securedata.cpp
LOCAL_CFLAGS += -g
include $(BUILD_SHARED_LIBRARY)
