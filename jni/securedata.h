#ifndef SECUREDATA_H
#define SECUREDATA_H
#include <openssl/bio.h>
#define DEBUG_LOG(tag, str, len) do { \
	FILE *f = fopen("/data/local/tmp/cryptlog.txt", "a+"); \
	fprintf(f, tag); \
	BIO_dump_fp(f, str, len); \
	  fflush(f); \
	  fclose(f); \
} while (0)

typedef struct {
	unsigned char *plainText;
	int plainTextLen;
	unsigned char *iv;
	int ivLen;
	unsigned char *aad;
	int aadLen;
	unsigned char *salt;
	int saltLen; 
	unsigned char *password;
	int passwdLen; 
	int keyLen; 
	int iterations; 
} EncryptInput;

typedef struct {
	unsigned char *cipherText;
	int cipherTextLen;
	unsigned char *tag;
	int tagLen;
} EncryptOutput;

typedef struct {
	unsigned char *cipherText;
	int cipherTextLen;
	unsigned char *iv;
	int ivLen;
	unsigned char *aad;
	int aadLen;
	unsigned char *tag;
	int tagLen;
	unsigned char *salt;
	int saltLen; 
	unsigned char *password;
	int passwdLen; 
	int keyLen; 
	int iterations; 
} DecryptInput;

typedef struct {
	unsigned char *plainText;
	int plainTextLen;
} DecryptOutput;

enum {
	E_SUCCESS = 0,
	E_INVALID_INPUT = 1,
	E_AUTH_VERIFICATION_FAILED = 2
};

int aes_gcm_encrypt(EncryptInput *encInput, EncryptOutput *encOutput);
int aes_gcm_decrypt(DecryptInput *decInput, DecryptOutput *decOutput);
#endif
