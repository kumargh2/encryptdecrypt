/*
 * AES GCM test program to test encryption and decryption.
 * This same program will be expanded for actual encryption and decryption.
 */

#include <stdio.h>
#include <stdlib.h>
#include <securedata.h>
#include <openssl/bio.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <android/log.h>

/* AES-GCM test data from NIST public test vectors */

static const unsigned char gcm_key[] = {
	0xee,0xbc,0x1f,0x57,0x48,0x7f,0x51,0x92,0x1c,0x04,0x65,0x66,
	0x5f,0x8a,0xe6,0xd1,0x65,0x8b,0xb2,0x6d,0xe6,0xf8,0xa0,0x69,
	0xa3,0x52,0x02,0x93,0xa5,0x72,0x07,0x8f
};

static const unsigned char gcm_iv[] = {
	0x99,0xaa,0x3e,0x68,0xed,0x81,0x73,0xa0,0xee,0xd0,0x66,0x84
};

static const unsigned char gcm_pt[] = {
	0xf5,0x6e,0x87,0x05,0x5b,0xc3,0x2d,0x0e,0xeb,0x31,0xb2,0xea,
	0xcc,0x2b,0xf2,0xa5
};

static const unsigned char gcm_aad[] = {
	0x4d,0x23,0xc3,0xce,0xc3,0x34,0xb4,0x9b,0xdb,0x37,0x0c,0x43,
	0x7f,0xec,0x78,0xde
};

static const unsigned char gcm_ct[] = {
	0xf7,0x26,0x44,0x13,0xa8,0x4c,0x0e,0x7c,0xd5,0x36,0x86,0x7e,
	0xb9,0xf2,0x17,0x36
};

static const unsigned char gcm_tag[] = {
	0x67,0xba,0x05,0x10,0x26,0x2a,0xe4,0x87,0xd7,0x37,0xee,0x62,
	0x98,0xf7,0x7e,0x0c
};
#define FREE(buf)	if (buf) free(buf);buf = NULL;
#if 0
typedef struct {
	char *plainText,
	int plainTextLen,
	char *iv,
	int ivLen,
	char *aad,
	int aadlen,
	char *key,
	int keyLen 
} EncryptInput;

typedef struct {
	char *cipherText,
	int cipherTextLen,
	char *tag,
	int tagLen
} EncryptOutput;

typedef struct {
	char *cipherText,
	int cipherTextLen,
	char *iv,
	int ivLen,
	char *aad,
	int aadlen,
	char *key,
	int keyLen, 
	char *tag,
	int tagLen
} DecryptInput;

typedef struct {
	char *plainText,
	int plainTextLen,
} DecryptOutput;
#endif
static int validateEncryptBufs(EncryptInput *encInput, EncryptOutput *encOutput) {
	if (!encInput || !encOutput) {
		return (E_INVALID_INPUT);
	}

	if (!encInput->plainText || !encInput->iv || !encInput->aad ||
		!encOutput->cipherText || !encOutput->tag) {
		return (E_INVALID_INPUT);
	}
}

static int validateDecryptBufs(DecryptInput *decInput, DecryptOutput *decOutput) {
	if (!decInput || !decOutput) {
		return (E_INVALID_INPUT);
	}

	if (!decInput->cipherText || !decInput->iv || !decInput->aad ||
		!decOutput->plainText || !decInput->tag) {
		return (E_INVALID_INPUT);
	}
}

int generateKeyFromPassword(const char *pass, int passlen,
			   unsigned char *salt, int saltlen, int iter,
			   int keylen, unsigned char *key)
{
    int ret = -1;
    //TODO: Validate the Input
    // Allocate and generate random salt.
    salt = (unsigned char *)calloc(1, saltlen);
    if (salt) {
    	int ret = RAND_bytes(salt, saltlen);
		if (ret != 1) {
			unsigned long errCode = 0;
			if ((errCode = ERR_get_error()) != 0) {
				char err[120];  // specs require string at
					// least 120 bytes long..
				__android_log_write(ANDROID_LOG_WARN,
				"Seed Generation Error",
				ERR_error_string(errCode, err));
			}
			FREE(salt);	
			return (-1);
		}
	} else {
		return (ret);
	}

	// Allocate memory for key and generate random salt.
	key = (unsigned char *)calloc(1, keylen);
	if (key) {
		ret = PKCS5_PBKDF2_HMAC(pass, passlen, salt, saltlen,
			 iter, EVP_sha256(), keylen, key);
		if (ret < 0) {
			char err[5];
			sprintf(err, "%d", ret);
			__android_log_write(ANDROID_LOG_WARN,
				"Password Generation Error", err);
			return (ret);
		}	
	} else {
		return (-1);
	}

	return (ret);
}

int aes_gcm_encrypt(EncryptInput *encInput, EncryptOutput *encOutput)
{
	EVP_CIPHER_CTX *ctx = NULL;
	int outlen = 0;
	unsigned char *tmpCipherText = NULL;
	unsigned char *key = NULL;
	int ret = -1;

	if ((ret = validateEncryptBufs(encInput, encOutput)) < 0) {
		// TODO: Throw the errors in android logcat.
		return (ret);
	}

	// Allocate memory for key and generate random salt.
	key = (unsigned char *)calloc(1, encInput->keyLen);
	if (key) {
	__android_log_write(ANDROID_LOG_WARN, "securedata enc passwd", (const char *)encInput->password);
	char pwdLen[5];
	sprintf(pwdLen, "%d", encInput->passwdLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata passwdLen", pwdLen);
	ret = PKCS5_PBKDF2_HMAC((const char *)encInput->password, encInput->passwdLen,
		encInput->salt, 22, encInput->iterations,
		 EVP_sha256(), 256, key);
	__android_log_write(ANDROID_LOG_WARN, "securedata salt", (const char *)encInput->salt);
	char saltLen[5];
	sprintf(saltLen, "%d", encInput->saltLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata saltLen", saltLen);
	char iter[5];
	sprintf(iter, "%d", encInput->iterations);
	__android_log_write(ANDROID_LOG_WARN, "securedata iterations", iter);
		ret = PKCS5_PBKDF2_HMAC((const char *)encInput->password, encInput->passwdLen,
			encInput->salt, 22, encInput->iterations,
			 EVP_sha256(), 256, key);
	char keyLen[5];
	sprintf(keyLen, "%d", encInput->keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata key", keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata key", (const char *)key);
		ret = PKCS5_PBKDF2_HMAC((const char *)encInput->password, encInput->passwdLen,
			encInput->salt, 22, encInput->iterations,
			 EVP_sha256(), 256, key);
	sprintf(keyLen, "%d", encInput->keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata key", keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata key", (const char *)key);
		if (ret < 0) {
			char err[5];
			sprintf(err, "%d", ret);
			__android_log_write(ANDROID_LOG_WARN,
				"Password Generation Error", err);
			return (ret);
		}	
	} else {
		return (-1);
	}
	
	encOutput->cipherText= (unsigned char *)calloc(1, 256);
	encOutput->tag= (unsigned char *)calloc(1, 256);
	tmpCipherText = encOutput->cipherText;
	//DEBUG_LOG("AES GCM Encrypt Plaintext:\n", (const char *)encInput->plainText, encInput->plainTextLen);
	ctx = EVP_CIPHER_CTX_new();

	/* Set cipher type and mode */
	EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	/* Set IV length if default 96 bits is not appropriate */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, encInput->ivLen, NULL);

	/* Initialise key and IV */
	EVP_EncryptInit_ex(ctx, NULL, NULL, (unsigned char const *)key, (unsigned char const*)encInput->iv);

	// DEBUG_LOG("after setting key and iv:\n",
	//			"Some", 4);
	/* Zero or more calls to specify any AAD */
	// TODO: Not sure where to use output len in the below case.
	// EVP_EncryptUpdate(ctx, NULL, &outlen,
	//		(unsigned char const *)encInput->aad,
	//		sizeof(encInput->aadLen));

	// DEBUG_LOG("before encrypting  Text:\n",
	//			"Some", 4);
	encOutput->cipherTextLen = 0;
	/* Encrypt plaintext */
	for (int i = 0; i < encInput->plainTextLen; i += 16) {
		int cipherTextLen = 0;
		int blockLen = 16;
		if (i > encInput->plainTextLen) {
			blockLen =  encInput->plainTextLen % 16;
		}
		// DEBUG_LOG("Before encrypt:\n", encInput->plainText,
		//		encInput->plainTextLen);
		EVP_EncryptUpdate(ctx, 
				(unsigned char *)tmpCipherText,
				&cipherTextLen,
				(unsigned char const *)(encInput->plainText + i), blockLen);
		// DEBUG_LOG("After encrypt1:\n", tmpCipherText,
		//		cipherTextLen);
		tmpCipherText += cipherTextLen;	
		encOutput->cipherTextLen += cipherTextLen;
	}
	/* Output encrypted block */
	// DEBUG_LOG("Cipher Text:\n", (const char *)encOutput->cipherText,
	//			encOutput->cipherTextLen);
//	__android_log_write(ANDROID_LOG_INFO, "tag here", "message here");
	/* Finalise: note get no output for GCM */
	EVP_EncryptFinal_ex(ctx, (unsigned char *)tmpCipherText,
			&outlen);
	tmpCipherText += outlen;
	encOutput->cipherTextLen += outlen;
	char outLen[5];
	sprintf(outLen, "%d",  encOutput->cipherTextLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata", (const char *)outLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata", (const char *)encOutput->cipherText);
	/* Get tag */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16,
			encOutput->tag);
	//__android_log_write(ANDROID_LOG_WARN, "securedata tag", (const char *)encOutput->tag);
	encOutput->tagLen = 16;
	/* Output tag */
	// DEBUG_LOG("Tag:\n", (const char *)tmpCipherText, 16);

	EVP_CIPHER_CTX_free(ctx);
	return (E_SUCCESS);
}

int aes_gcm_decrypt(DecryptInput *decInput, DecryptOutput *decOutput)
{
	EVP_CIPHER_CTX *ctx = NULL;
	int outlen = 0, rv;
	int ret = -1;
	unsigned char* key = NULL;

//	if ((ret = validateDecryptBufs(decInput, decOutput)) < 0) {
		// TODO: Throw the errors in android logcat.
//		return (ret);
//	}
	// Allocate memory for key and generate random salt.
	key = (unsigned char *)calloc(1, decInput->keyLen);
	if (key) {
		ret = PKCS5_PBKDF2_HMAC((const char *)decInput->password, decInput->passwdLen,
			decInput->salt, 22, decInput->iterations,
			 EVP_sha256(), 256, key);
	__android_log_write(ANDROID_LOG_WARN, "securedata dec passwd", (const char *)decInput->password);
	char pwdLen[5];
	sprintf(pwdLen, "%d", decInput->passwdLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata passwdLen", pwdLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata salt", (const char *)decInput->salt);
	char saltLen[5];
	sprintf(saltLen, "%d", decInput->saltLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata saltLen", saltLen);
	char iter[5];
	sprintf(iter, "%d", decInput->iterations);
	__android_log_write(ANDROID_LOG_WARN, "securedata iterations", iter);
	char keyLen[5];
	sprintf(keyLen, "%d", decInput->keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata keyLen", keyLen);
	__android_log_write(ANDROID_LOG_WARN, "securedata key", (const char *)key);
		if (ret < 0) {
			char err[5];
			sprintf(err, "%d", ret);
			__android_log_write(ANDROID_LOG_WARN,
				"Password Generation Error", err);
			return (ret);
		}	
	} else {
		return (-1);
	}
	decOutput->plainText= (unsigned char *)calloc(1, 256);
	unsigned char *tmpData = decOutput->plainText;

	// DEBUG_LOG("Cipher Text:\n", (const char *)decInput->cipherText,
//			decInput->cipherTextLen);
	ctx = EVP_CIPHER_CTX_new();
	/* Select cipher */
	EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	/* Set IV length, omit for 96 bits */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN,
			decInput->ivLen, NULL);
	/* Specify key and IV */
	EVP_DecryptInit_ex(ctx, NULL, NULL,
			(unsigned char const *)key,
			(unsigned char const*)decInput->iv);
#if 1
	/* Set expected tag value. A restriction in OpenSSL 1.0.1c and earlier
         * required the tag before any AAD or ciphertext */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, decInput->tagLen, decInput->tag);
#endif
	/* Zero or more calls to specify any AAD */
	// TODO: What to do with outlen here?
	//EVP_DecryptUpdate(ctx, NULL, &outlen, (unsigned char const*)decInput->aad,
	//		decInput->aadLen);
	/* Decrypt plaintext */
	/* Encrypt plaintext */
	__android_log_write(ANDROID_LOG_WARN, "Decrypt Cipher Data",
		 (const char *)decInput->cipherText);
	//__android_log_write(ANDROID_LOG_WARN, "Decrypt Cipher Data",
	//	 (const char *)decInput->tag);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt Key",
		 (const char *)key);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt IV",
		 (const char *)decInput->iv);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt aad",
		 (const char *)decInput->aad);
	char cipherLen[5];
	sprintf(cipherLen, "%d",  decInput->cipherTextLen);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt cipherLen", cipherLen);
	char tagLen[5];
	sprintf(tagLen, "%d",  decInput->tagLen);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt tagLen", tagLen);
	char aadLen[5];
	sprintf(aadLen, "%d",  decInput->aadLen);
	__android_log_write(ANDROID_LOG_WARN, "Decrypt aadLen", aadLen);
	for (int i = 0; i < decInput->cipherTextLen; i += 16) {
		int plainTextLen = 0;
		int blockLen = 16;
		if (i > decInput->cipherTextLen) {
			blockLen =  decInput->cipherTextLen % 16;
		}
		ret = EVP_DecryptUpdate(ctx,
				(unsigned char *)tmpData, 
				&plainTextLen,
				(unsigned char const *)(decInput->cipherText + i),
				blockLen);
		char retVal[5];
		sprintf(retVal, "%d", ret);
		__android_log_write(ANDROID_LOG_WARN, "update RetVal", retVal);
		tmpData += plainTextLen;
		decOutput->plainTextLen += plainTextLen;
	}
	/* Output decrypted block */
	// DEBUG_LOG("Plaintext:\n", (char *)decOutput->plainText,
	//		decOutput->plainTextLen);
	/* Set expected tag value. Works in OpenSSL 1.0.1d and later */
	ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, decInput->tagLen,
	 (void *)decInput->tag);
	char retVal[5];
	sprintf(retVal, "%d", ret);
	__android_log_write(ANDROID_LOG_WARN, "RetVal", retVal);
	/* Finalise: note get no output for GCM */
	rv = EVP_DecryptFinal_ex(ctx, (unsigned char *)tmpData, &outlen);
	/* Print out return value. If this is not successful authentication
	 * failed and plaintext is not trustworthy.
	 */
	EVP_CIPHER_CTX_free(ctx);
	__android_log_write(ANDROID_LOG_WARN, "Data", (const char *)decOutput->plainText);
	__android_log_write(ANDROID_LOG_WARN, "Data Val", "Data");
	if (rv > 0) {
		// DEBUG_LOG("Auth verification", "Tag Success", 11);
		__android_log_print(ANDROID_LOG_INFO, "Tag Verify", "Successful");
		return (E_SUCCESS);
	} else {
		// DEBUG_LOG("Auth verification", "Tag Failed", 10);
		__android_log_print(ANDROID_LOG_INFO, "Tag Verify", "Failed");
		return (E_AUTH_VERIFICATION_FAILED);
	}
}
#if 0
void aes_gcm_encrypt()
{
	EVP_CIPHER_CTX *ctx;
	int outlen, tmplen;
	unsigned char outbuf[1024];
	printf("AES GCM Encrypt:\n");

	// BIO_dump_fp(stdout, (const char *)gcm_pt, sizeof(gcm_pt));
	DEBUG_LOG("Plain Text:\n", (const char *)gcm_pt, sizeof(gcm_pt));
	ctx = EVP_CIPHER_CTX_new();

	/* Set cipher type and mode */
	EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	/* Set IV length if default 96 bits is not appropriate */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, sizeof(gcm_iv), NULL);
	/* Initialise key and IV */
	EVP_EncryptInit_ex(ctx, NULL, NULL, gcm_key, gcm_iv);
	/* Zero or more calls to specify any AAD */
	EVP_EncryptUpdate(ctx, NULL, &outlen, gcm_aad, sizeof(gcm_aad));
	/* Encrypt plaintext */
	EVP_EncryptUpdate(ctx, outbuf, &outlen, gcm_pt, sizeof(gcm_pt));
	/* Output encrypted block */
	// printf("Ciphertext:\n");
	// BIO_dump_fp(stdout, (const char *)outbuf, outlen);
	DEBUG_LOG("Cipher Text:\n", (const char *)outbuf, outlen);
	/* Finalise: note get no output for GCM */
	EVP_EncryptFinal_ex(ctx, outbuf, &outlen);
	/* Get tag */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, outbuf);
	/* Output tag */
	// printf("Tag:\n");
	// BIO_dump_fp(stdout, (const char *)outbuf, 16);
	DEBUG_LOG("Tag;\n", (const char *)outbuf, 16);
	EVP_CIPHER_CTX_free(ctx);
}

void aes_gcm_decrypt(void)
{
	EVP_CIPHER_CTX *ctx;
	int outlen, tmplen, rv;
	unsigned char outbuf[1024];
	// printf("AES GCM Derypt:\n");
	// printf("Ciphertext:\n");

	// BIO_dump_fp(stdout, (const char *)gcm_ct, sizeof(gcm_ct));
	DEBUG_LOG("AES GCP Decrypt; Cipher Text:\n", (const char *)gcm_ct,
			sizeof(gcm_ct));
	ctx = EVP_CIPHER_CTX_new();
	/* Select cipher */
	EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	/* Set IV length, omit for 96 bits */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, sizeof(gcm_iv), NULL);
	/* Specify key and IV */
	EVP_DecryptInit_ex(ctx, NULL, NULL, gcm_key, gcm_iv);
#if 1
	/* Set expected tag value. A restriction in OpenSSL 1.0.1c and earlier
         * required the tag before any AAD or ciphertext */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, sizeof(gcm_tag), (void *)gcm_tag);
#endif
	/* Zero or more calls to specify any AAD */
	EVP_DecryptUpdate(ctx, NULL, &outlen, gcm_aad, sizeof(gcm_aad));
	/* Decrypt plaintext */
	EVP_DecryptUpdate(ctx, outbuf, &outlen, gcm_ct, sizeof(gcm_ct));
	/* Output decrypted block */
	// printf("Plaintext:\n");
	// BIO_dump_fp(stdout, (char *)outbuf, outlen);
	char mybuf[10];
	memset(mybuf, 0, 10);
	sprintf(mybuf, "%d", outlen);
	DEBUG_LOG("Plain Text len:\n", mybuf, 10);
	DEBUG_LOG("Plain Text:\n", (char *)outbuf, outlen);
	/* Set expected tag value. Works in OpenSSL 1.0.1d and later */
	// EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, sizeof(gcm_tag),
	// (void *)gcm_tag);
	/* Finalise: note get no output for GCM */
	rv = EVP_DecryptFinal_ex(ctx, outbuf, &outlen);
	/* Print out return value. If this is not successful authentication
	 * failed and plaintext is not trustworthy.
	 */
	// printf("Tag Verify %s\n", rv > 0 ? "Successful!" : "Failed!");
	if (rv > 0) {
		DEBUG_LOG("Auth verification", "Tag Success", 11);
		//__android_log_print(ANDROID_LOG_INFO, "Tag Verify", "Successful");
	} else {
		DEBUG_LOG("Auth verification", "Tag Failed", 10);
		//__android_log_print(ANDROID_LOG_INFO, "Tag Verify", "Failed");
	}
	EVP_CIPHER_CTX_free(ctx);
}
int main(int argc, char **argv)
{
	aes_gcm_encrypt();
	aes_gcm_decrypt();
}
#endif
