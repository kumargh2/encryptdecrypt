#include <jni.h>
#include <com_example_security_HelloSecurity.h>
#include <securedata.h>
#include <stdio.h>
#include <android/log.h>

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
#if 0
jstring Java_com_example_security_HelloSecurity_versionFromOpenssl
  (JNIEnv *env, jobject thiz)
{
	aes_gcm_encrypt();
	aes_gcm_decrypt();
    return (env)->NewStringUTF("Hello from JNI !");
}

#endif
jstring Java_com_example_security_HelloSecurity_versionFromOpenssl
  (JNIEnv *env, jobject thiz)
{
	// aes_gcm_encrypt();
	// aes_gcm_decrypt();
    return env->NewStringUTF("Hello from JNI !");
}

jobject Java_com_example_security_HelloSecurity_encryptData (JNIEnv *env, jobject thiz, jobject input)
{
	EncryptInput encryptInput;
	EncryptOutput encryptOutput;

	memset(&encryptInput, 0, sizeof(EncryptInput));
	memset(&encryptOutput, 0, sizeof(EncryptOutput));
	//	DEBUG_LOG("encryptData\n", "Enter Encrypt", 13);	
	// 1. Get the fields from the java object.
	jclass clazz;
	clazz = env->GetObjectClass(input);
	// 2. Translate the java fields into C/C++ fields.
	
	//Plain Text.
	jfieldID instanceFieldId;
	instanceFieldId = env->GetFieldID(clazz,
			    "nPlainText", "Ljava/lang/String;");
	jstring nPlainText;
	nPlainText = (jstring)env->GetObjectField(input, instanceFieldId);
	encryptInput.plainText = (unsigned char *)env->GetStringUTFChars(nPlainText, 0);
	//DEBUG_LOG("Plain Text:\n", encryptInput.plainText, 16);	
	
	//Plain Text Len.
	instanceFieldId = env->GetFieldID(clazz,
			    "nPlainTextLen", "I");
	encryptInput.plainTextLen = (int)env->GetIntField(input, instanceFieldId);
	char plainTextLen[5];
	sprintf(plainTextLen, "%d",  encryptInput.plainTextLen);
	// DEBUG_LOG("Plain Text Len:\n", plainTextLen, 4);	
	
	// Password
	instanceFieldId = env->GetFieldID(clazz,
			    "password", "Ljava/lang/String;");
	jstring nPassword;
	nPassword = (jstring)env->GetObjectField(input, instanceFieldId);
	encryptInput.password = (unsigned char *)env->GetStringUTFChars(nPassword, 0);

	// Password Len	
	instanceFieldId = env->GetFieldID(clazz,
			    "passwdLen", "I");
	encryptInput.passwdLen = (int)env->GetIntField(input, instanceFieldId);
	
	// Salt
	instanceFieldId = env->GetFieldID(clazz,
			    "salt", "Ljava/lang/String;");
	jstring nSalt;
	nSalt = (jstring)env->GetObjectField(input, instanceFieldId);
	encryptInput.salt = (unsigned char *)env->GetStringUTFChars(nSalt, 0);

	// Salt Len	
	instanceFieldId = env->GetFieldID(clazz,
			    "saltLen", "I");
	encryptInput.saltLen = (int)env->GetIntField(input, instanceFieldId);

	// iterations	
	instanceFieldId = env->GetFieldID(clazz,
			    "iterations", "I");
	encryptInput.iterations = (int)env->GetIntField(input, instanceFieldId);

	//iv.
	instanceFieldId = env->GetFieldID(clazz,
			    "nIv", "Ljava/lang/String;");
	jstring nIv = (jstring)env->GetObjectField(input, instanceFieldId);
	encryptInput.iv = (unsigned char *)env->GetStringUTFChars(nIv, 0);
	// DEBUG_LOG("iv:\n", encryptInput.iv, 16);	
	
	//iv len.
	instanceFieldId = env->GetFieldID(clazz, "nIvLen", "I");
	encryptInput.ivLen = (int)env->GetIntField(input, instanceFieldId);
	char ivLen[5];
	sprintf(ivLen, "%d",  encryptInput.ivLen);
	// DEBUG_LOG("ivlen:\n", ivLen, 4);	
	
	//aad.
	instanceFieldId = env->GetFieldID(clazz,
			    "nAad", "Ljava/lang/String;");
	jstring nAad = (jstring)env->GetObjectField(input, instanceFieldId);
	encryptInput.aad = (unsigned char *)env->GetStringUTFChars(nAad, 0);
	// DEBUG_LOG("aad:\n", encryptInput.aad, 16);	
	
	//aad len.
	//instanceFieldId = env->GetFieldID(clazz, "nAadLen", "I");
	//encryptInput.aadLen = (int)env->GetIntField(input, instanceFieldId);
	char aadLen[5];
	//sprintf(aadLen, "%d",  encryptInput.aadLen);
	// DEBUG_LOG("aad Len:\n", aadLen, 4);	
	sprintf(aadLen, "%d",  4);
	
	//key.
	//instanceFieldId = env->GetFieldID(clazz,
	//		    "nKey", "Ljava/lang/String;");
	//jstring nKey = (jstring)env->GetObjectField(input, instanceFieldId);
	//encryptInput.key = (char *)env->GetStringUTFChars(nKey, 0);
	// DEBUG_LOG("key:\n", encryptInput.key, 32);	
	
	//Key len.
	instanceFieldId = env->GetFieldID(clazz, "nKeyLen", "I");
	encryptInput.keyLen = (int)env->GetIntField(input, instanceFieldId);
	char keyLen[5];
	sprintf(keyLen, "%d",  encryptInput.keyLen);
	// DEBUG_LOG("key len:\n", keyLen, 4);	

	   // use your string
	   //
	   //    (*env)->ReleaseStringUTFChars(env, javaString, nativeString);
	// 3. Send the aes_gcm_encrypt request.
	 aes_gcm_encrypt(&encryptInput, &encryptOutput);
	// DEBUG_LOG("After aes_gcm_encrypt:\n", "AES", 3);	
	// 4. Convert the output into java fields.
	jclass outputClass = env->FindClass("com/example/security/NativeEncryptOutput");
	// Get the Method ID of the constructor which takes encrypted
	// Output format 
	jmethodID outputCId = env->GetMethodID(outputClass, "<init>",
			"([BI[BI)V");
	if (NULL == outputCId) return NULL;
	// Call back constructor to allocate a new instance, with an int argument
	char cipherLen[5];
	sprintf(cipherLen, "%d", encryptOutput.cipherTextLen);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android", cipherLen);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android", (const char*)encryptOutput.cipherText);
	jbyteArray encText = env->NewByteArray(encryptOutput.cipherTextLen);
	env->SetByteArrayRegion(encText, 0, encryptOutput.cipherTextLen,
				(const jbyte *)encryptOutput.cipherText);
	char *ct  = (char *)env->GetByteArrayElements(encText, NULL);
	__android_log_write(ANDROID_LOG_WARN, "Encrypted cipherText", ct);
	char tagLen[5];
	sprintf(tagLen, "%d", encryptOutput.tagLen);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android tag", tagLen);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android", (const char *)encryptOutput.tag);
	char *ct3  = (char *)env->GetByteArrayElements(encText, NULL);
	__android_log_write(ANDROID_LOG_WARN, "Encrypted cipherText3", ct3);
	jbyteArray tagText = env->NewByteArray(encryptOutput.tagLen);
	env->SetByteArrayRegion(tagText, 0, encryptOutput.tagLen,
				(const jbyte*)encryptOutput.tag);
	char *ct2  = (char *)env->GetByteArrayElements(encText, NULL);
	__android_log_write(ANDROID_LOG_WARN, "Encrypted cipherText2", ct2);
	jobject encOutputObj = env->NewObject(outputClass, outputCId,
			encText, //encryptOutput.cipherText,
			encryptOutput.cipherTextLen,
			tagText, encryptOutput.tagLen);
	char *ct1  = (char *)env->GetByteArrayElements(encText, NULL);
	__android_log_write(ANDROID_LOG_WARN, "Encrypted cipherText1", "Hehe");
			//encryptOutput.tagLen);
	// 5. Free the allocated memory.
	// char cipherLen[5];
	//sprintf(cipherLen, "%d", encryptOutput.cipherTextLen);
	//__android_log_write(ANDROID_LOG_WARN, "openssl-android", cipherLen);

	env->ReleaseStringUTFChars(nPlainText, (const char *)encryptInput.plainText);
	env->ReleaseStringUTFChars(nIv, (const char *)encryptInput.iv);
	env->ReleaseStringUTFChars(nAad, (const char *)encryptInput.aad);
	// env->ReleaseStringUTFChars(nKey, encryptInput.key);

	//free(encryptOutput.cipherText);
	//free(encryptOutput.tag);
	// 5. Return the output.
	return (encOutputObj);
}

jobject Java_com_example_security_HelloSecurity_decryptData (JNIEnv *env, jobject thiz, jobject input)
{
	DecryptInput decryptInput;
	DecryptOutput decryptOutput;

	// 1. Get the fields from the java object.
	jclass clazz;
	clazz = env->GetObjectClass(input);
	// 2. Translate the java fields into C/C++ fields.

	__android_log_write(ANDROID_LOG_WARN, "openssl-android deryptData", "Enter");
	//Cipher Text.
	jfieldID instanceFieldId;
	instanceFieldId = env->GetFieldID(clazz,
			    "nCipherText", "[B");
	__android_log_write(ANDROID_LOG_WARN, "openssl-android deryptData", "Enter1");
	jbyteArray nCipherText = (jbyteArray)env->GetObjectField(input, instanceFieldId);
	decryptInput.cipherText = (unsigned char *)env->GetByteArrayElements(nCipherText, NULL);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android cipherText", (const char *)decryptInput.cipherText);
	//Cipher Text Len.
	instanceFieldId = env->GetFieldID(clazz,
			    "nCipherTextLen", "I");
	decryptInput.cipherTextLen = (int)env->GetIntField(input, instanceFieldId);

	// Password
	instanceFieldId = env->GetFieldID(clazz,
			    "password", "Ljava/lang/String;");
	jstring nPassword;
	nPassword = (jstring)env->GetObjectField(input, instanceFieldId);
	decryptInput.password = (unsigned char *)env->GetStringUTFChars(nPassword, 0);

	// Password Len	
	instanceFieldId = env->GetFieldID(clazz,
			    "passwdLen", "I");
	decryptInput.passwdLen = (int)env->GetIntField(input, instanceFieldId);
	
	// Salt
	instanceFieldId = env->GetFieldID(clazz,
			    "salt", "Ljava/lang/String;");
	jstring nSalt;
	nSalt = (jstring)env->GetObjectField(input, instanceFieldId);
	decryptInput.salt = (unsigned char *)env->GetStringUTFChars(nSalt, 0);

	// Salt Len	
	instanceFieldId = env->GetFieldID(clazz,
			    "saltLen", "I");
	decryptInput.saltLen = (int)env->GetIntField(input, instanceFieldId);

	// iterations	
	instanceFieldId = env->GetFieldID(clazz,
			    "iterations", "I");
	decryptInput.iterations = (int)env->GetIntField(input, instanceFieldId);
	
	//__android_log_write(ANDROID_LOG_WARN, "openssl-android cipher text len", decryptInput.cipherTextLen);
	//iv.
	instanceFieldId = (env)->GetFieldID(clazz,
			    "nIv", "Ljava/lang/String;");
	jstring nIv = (jstring)env->GetObjectField(input, instanceFieldId);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android deryptData", "Enter3");
	decryptInput.iv = (unsigned char *)env->GetStringUTFChars(nIv, 0);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android deryptData", "Enter4");
	__android_log_write(ANDROID_LOG_WARN, "openssl-android iv", (const char *)decryptInput.iv);
	
	//iv len.
	instanceFieldId = (env)->GetFieldID(clazz, "nIvLen", "I");
	decryptInput.ivLen = (int)env->GetIntField(input, instanceFieldId);
	
	//aad.
	instanceFieldId = (env)->GetFieldID(clazz,
			    "nAad", "Ljava/lang/String;");
	jstring nAad = (jstring)env->GetObjectField(input, instanceFieldId);
	decryptInput.aad = (unsigned char *)env->GetStringUTFChars(nAad, 0);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android aad", (const char *)decryptInput.aad);
	
	//aad len.
	instanceFieldId = env->GetFieldID(clazz, "nAadLen", "I");
	decryptInput.aadLen = (int)env->GetIntField(input, instanceFieldId);
	
	//key.
	// instanceFieldId = (env)->GetFieldID(clazz,
	//			    "nKey", "Ljava/lang/String;");
	// jstring nKey = (jstring)env->GetObjectField(input, instanceFieldId);
	// decryptInput.key = (char *)env->GetStringUTFChars(nKey, 0);
	// __android_log_write(ANDROID_LOG_WARN, "openssl-android key", decryptInput.key);
	
	//Key len.
	instanceFieldId = env->GetFieldID(clazz, "nKeyLen", "I");
	decryptInput.keyLen = (int)env->GetIntField(input, instanceFieldId);

	// Tag.
	instanceFieldId = (env)->GetFieldID(clazz,
			    "nTag", "[B");
	jbyteArray nTag = (jbyteArray)env->GetObjectField(input, instanceFieldId);
	decryptInput.tag = (unsigned char *)env->GetByteArrayElements(nTag, NULL);
	__android_log_write(ANDROID_LOG_WARN, "openssl-android tag", (const char *)decryptInput.tag);
	
	//Tag len.
	instanceFieldId = env->GetFieldID(clazz, "nTagLen", "I");
	decryptInput.tagLen = (int)env->GetIntField(input, instanceFieldId);

	   // use your string
	   //
	   //    (*env)->ReleaseStringUTFChars(env, javaString, nativeString);
	// 3. Send the aes_gcm_encrypt request.
	aes_gcm_decrypt(&decryptInput, &decryptOutput);
	// 4. Convert the output into java fields.
	jclass outputClass = env->FindClass("com/example/security/NativeDecryptOutput");
	// Get the Method ID of the constructor which takes encrypted
	// Output format 
	jmethodID outputCId = env->GetMethodID(outputClass, "<init>",
			"(Ljava/lang/String;I)V");
	if (NULL == outputCId) return NULL;
	// Call back constructor to allocate a new instance, with an int argument
	jobject decOutputObj; 
	
	if (decryptOutput.plainTextLen != 0) {
		decOutputObj = env->NewObject(outputClass, outputCId,
			decryptOutput.plainText, decryptOutput.plainTextLen);
	}
	// 5. Free the allocated memory.
	env->ReleaseByteArrayElements(nTag, (jbyte *)decryptInput.tag, 0);
	//env->ReleaseByteArrayElements(nCipherText, (jbyte *)decryptInput.cipherText, 0);
	env->ReleaseStringUTFChars(nIv, (const char *)decryptInput.iv);
	env->ReleaseStringUTFChars(nAad, (const char *)decryptInput.aad);
	//env->ReleaseStringUTFChars(nKey, decryptInput.key);
//	free(decryptOutput.plainText);
	// 5. Return the output.
	return (decOutputObj);
}
