#!/bin/bash
#
# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#
# This script imports new versions of OpenSSL (http://openssl.org/source) into the
# Android source tree.  To run, (1) fetch the appropriate tarball from the OpenSSL repository,
# (2) check the gpg/pgp signature, and then (3) run:
#   ./import_openssl.sh import openssl-*.tar.gz
#
# IMPORTANT: See README.android for additional details.

# turn on exit on error as well as a warning when it happens
function die() {
  declare -r message=$1

  echo $message
  exit 1
}

function usage() {
  declare -r message=$1

  if [ ! "$message" = "" ]; then
    echo $message
  fi
  echo "Usage:"
  echo "  ./import_openssl.sh import"
  exit 1
}

function main() {
  if [ ! -d patches ]; then
    die "OpenSSL patch directory patches/ not found"
  fi

  if [ ! -f openssl.version ]; then
    die "openssl.version not found"
  fi

  source openssl.version
  if [ "$OPENSSL_VERSION" == "" ]; then
    die "Invalid openssl.version; see README.android for more information"
  fi

  OPENSSL_DIR=openssl-$OPENSSL_VERSION
  OPENSSL_DIR_ORIG=$OPENSSL_DIR.orig

  if [ ! -f openssl.config ]; then
    die "openssl.config not found"
  fi

  source openssl.config
  if [ "$CONFIGURE_ARGS" == "" -o "$UNNEEDED_SOURCES" == "" -o "$NEEDED_SOURCES" == "" ]; then
    die "Invalid openssl.config; see README.android for more information"
  fi

  declare -r command=$1
  shift || usage "No command specified. Try import, regenerate, or generate."
  if [ "$command" = "import" ]; then
    import
  elif [ "$command" = "regenerate" ]; then
    declare -r patch=$1
    shift || usage "No patch file specified."
    [ -d $OPENSSL_DIR ] || usage "$OPENSSL_DIR not found, did you mean to use generate?"
    [ -d $OPENSSL_DIR_ORIG_ORIG ] || usage "$OPENSSL_DIR_ORIG not found, did you mean to use generate?"
    regenerate $patch
  elif [ "$command" = "generate" ]; then
    declare -r patch=$1
    shift || usage "No patch file specified."
    declare -r tar=$1
    shift || usage "No tar file specified."
    generate $patch $tar
  else
    usage "Unknown command specified $command. Try import, regenerate, or generate."
  fi
}

function import() {
  # Avoid checking in symlinks
  #for i in `find include/openssl -type l`; do
  #  target=`readlink $i`
  #  rm -f $i
  #  if [ -f include/openssl/$target ]; then
  #    cp include/openssl/$target $i
  #  fi
  #done
 ./Configure	
  # Copy Makefiles
  cp ./patches/apps_Android.mk apps/Android.mk
  cp ./patches/crypto_Android.mk crypto/Android.mk
  cp ./patches/ssl_Android.mk ssl/Android.mk

  # Generate arm asm
  perl crypto/aes/asm/aes-armv4.pl         > crypto/aes/asm/aes-armv4.s
  perl crypto/bn/asm/armv4-mont.pl         > crypto/bn/asm/armv4-mont.s
  perl crypto/sha/asm/sha1-armv4-large.pl  > crypto/sha/asm/sha1-armv4-large.s
  perl crypto/sha/asm/sha256-armv4.pl      > crypto/sha/asm/sha256-armv4.s
  perl crypto/sha/asm/sha512-armv4.pl      > crypto/sha/asm/sha512-armv4.s

  # Generate mips asm
  perl crypto/aes/asm/aes-mips.pl          > crypto/aes/asm/aes-mips.s
  perl crypto/bn/asm/mips.pl               > crypto/bn/asm/bn-mips.s
  perl crypto/bn/asm/mips-mont.pl          > crypto/bn/asm/mips-mont.s
  perl crypto/sha/asm/sha1-mips.pl         > crypto/sha/asm/sha1-mips.s
  perl crypto/sha/asm/sha512-mips.pl       > crypto/sha/asm/sha256-mips.s

  # Setup android.testssl directory
  mkdir android.testssl
  cat test/testssl | \
    sed 's#../util/shlib_wrap.sh ./ssltest#adb shell /system/bin/ssltest#' | \
    sed 's#../util/shlib_wrap.sh ../apps/openssl#adb shell /system/bin/openssl#' | \
    sed 's#adb shell /system/bin/openssl no-dh#[ `adb shell /system/bin/openssl no-dh` = no-dh ]#' | \
    sed 's#adb shell /system/bin/openssl no-rsa#[ `adb shell /system/bin/openssl no-rsa` = no-dh ]#' | \
    sed 's#../apps/server2.pem#/sdcard/android.testssl/server2.pem#' | \
    cat > \
    android.testssl/testssl
  chmod +x android.testssl/testssl
  cat test/Uss.cnf | sed 's#./.rnd#/sdcard/android.testssl/.rnd#' >> android.testssl/Uss.cnf
  cat test/CAss.cnf | sed 's#./.rnd#/sdcard/android.testssl/.rnd#' >> android.testssl/CAss.cnf
  cp apps/server2.pem android.testssl/
}

main $@
