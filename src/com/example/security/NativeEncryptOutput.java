package com.example.security;

import android.util.Log;
import java.util.Arrays;
public class NativeEncryptOutput {
	public  byte[] nCipherText;
	public int nCipherTextLen;
	public byte[] nTag;
	public int nTagLen;
	NativeEncryptOutput(byte[] cipherText, int textLen,
			byte[] tag, int tagLen) {
		if (textLen != 0 ) {	
			// nCipherText = new byte[textLen];
			Log.d("NEO cipher text 1", bytesToHex(cipherText));
			nCipherText = Arrays.copyOf(cipherText, textLen);
			//System.arraycopy(nCipherText, 0, cipherText, 0, textLen);
			Log.d("NEO cipher text", bytesToHex(nCipherText));
			this.nCipherTextLen = textLen;
		}
		if (tagLen != 0) {
			// nTag = new byte[tagLen];
			// System.arraycopy(nTag, 0, tag, 0, tagLen);
			nTag = Arrays.copyOf(tag, tagLen);
			Log.d("NEO ntag", bytesToHex(nTag));
			Log.d("NEO tag", bytesToHex(tag));
			this.nTagLen = tagLen;
		}
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
		v = bytes[j] & 0xFF;
		hexChars[j * 2] = hexArray[v >>> 4];
		hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}
