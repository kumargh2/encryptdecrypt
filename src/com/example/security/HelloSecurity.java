package com.example.security;

import android.app.Activity;
import android.widget.TextView;
import android.os.Bundle;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;


public class HelloSecurity extends Activity
{
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
		v = bytes[j] & 0xFF;
		hexChars[j * 2] = hexArray[v >>> 4];
		hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
    /** Called when the activity is first created. */
	    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
/*
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						//Yes button clicked
						NativeEncryptInput nei = new NativeEncryptInput();
						nei.nPlainText = "Hello My dear ku";
						nei.nPlainTextLen = nei.nPlainText.length();
						nei.nIv="This is a tes iv";
						nei.nIvLen = nei.nIv.length();
						nei.nAad="This is a te aad";
						nei.nAadLen=nei.nAad.length();
						nei.nKey="This is 256 bit key used for enc";
						nei.nKeyLen = 256;
						NativeEncryptOutput neo = encryptData(nei);
						Log.d("Encrypted Text", neo.nCipherText);
						Log.d("Encrypted Tag", neo.nTag);
						NativeDecryptInput ndi = new NativeDecryptInput();
						ndi.nCipherText = neo.nCipherText;
						ndi.nCipherTextLen = neo.nCipherText.length();
						ndi.nIv=nei.nIv;
						ndi.nIvLen = nei.nIv.length();
						ndi.nAad=nei.nAad;
						ndi.nAadLen=nei.nAad.length();
						ndi.nKey=nei.nKey;
						ndi.nKeyLen = 256;
						ndi.nTag = neo.nTag;
						ndi.nTagLen = neo.nTagLen;
						NativeDecryptOutput ndo = decryptData(ndi); 
						Log.d("Decrypted Text", ndo.nPlainText); 
					break;

					case DialogInterface.BUTTON_NEGATIVE:
						//No button clicked
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
*/
        XMLCreator.createXML();

			NativeEncryptInput nei = new NativeEncryptInput();
			nei.nPlainText = "Hello My dear ku";
			nei.nPlainTextLen = nei.nPlainText.length();
			nei.nIv="This is a tes iv";
			nei.nIvLen = nei.nIv.length();
			nei.nAad="This is a te aad";
			nei.nAadLen=nei.nAad.length();
			nei.salt = "This is salt used for enc";
			nei.saltLen = 256;
			nei.password = "Hello";
			nei.passwdLen = 5;
			nei.iterations = 1000;
			nei.nKeyLen = 256;
			Log.d("Before Encrypting", "Hai");
			NativeEncryptOutput neo = encryptData(nei);
			if (neo == null) {
				Log.d("Encrypted Text", null);
			} else {
				Log.d("Encrypted Text cipher len", "" + neo.nCipherTextLen);
				Log.d("Encrypted Text cipher", bytesToHex(neo.nCipherText));
				Log.d("Encrypted Text tag len", "" + neo.nTagLen);
			}
			if (neo.nCipherTextLen != 0) {
				NativeDecryptInput ndi = 
					new NativeDecryptInput(neo.nCipherText, neo.nCipherTextLen,
					nei.nIv, nei.nIv.length(), nei.nAad, nei.nAad.length(),
					256, neo.nTag, neo.nTagLen, nei.salt, nei.saltLen, nei.password,
					nei.passwdLen, nei.iterations);
				NativeDecryptOutput ndo = decryptData(ndi); 
				//Log.d("Decrypted Text", ndo.nPlainText); 
			}
			

	}

	/* This is another native method declaration that is *not*
     * implemented by 'hello-jni'. This is simply to show that
     * you can declare as many native methods in your Java code
     * as you want, their implementation is searched in the
     * currently loaded native libraries only the first time
     * you call them.
     *
     * Trying to call this function will result in a
     * java.lang.UnsatisfiedLinkError exception !
     */
    public native String  unimplementedVersionFromOpenssl();
    
    /*
     * Encrypt the Data
     */
	public native NativeEncryptOutput  encryptData(NativeEncryptInput nei);
    
    /*
     * Decrypt the Data
     */
	public native NativeDecryptOutput  decryptData(NativeDecryptInput ndi);


    /* this is used to load the 'hello-jni' library on application
     * startup. The library has already been unpacked into
     * /data/data/com.example.hellojni/lib/libhello-jni.so at
     * installation time by the package manager.
     */
    static {
        System.loadLibrary("crypto");
        System.loadLibrary("openssl-android");
    }
}
