package com.example.security;
import java.util.Arrays;
public class NativeDecryptInput {
	public byte[] nCipherText;
	public int nCipherTextLen;
	public String nIv;
	public int nIvLen;
	public String nAad;
	public int nAadLen;
	public byte[] nTag;
	public int nTagLen;
	public String salt;
	public int saltLen; 
	public String password;
	public int passwdLen; 
	public int iterations; 
	public int nKeyLen; 

	NativeDecryptInput(byte[] nCipher, int cipherLen, String nIv,
		int nIvLen, String nAad, int nAadLen, int nKeyLen,
		 byte[] tag, int tagLen, String salt, int saltLen,
		String password, int passwdLen, int iterations) {
		if (cipherLen != 0 ) {	
			nCipherText = Arrays.copyOf(nCipher, cipherLen);
			this.nCipherTextLen = cipherLen;
		}
		if (tagLen != 0) {
			nTag = Arrays.copyOf(tag, tagLen);
			this.nTagLen = tagLen;
		}
		this.nIv = nIv;
		this.nIvLen = nIvLen;
		this.nAad = nAad;
		this.nAadLen = nAadLen;
		this.salt = salt;
		this.saltLen = saltLen;
		this.password = password;
		this.passwdLen = passwdLen;
		this.iterations = iterations; 
		this.nKeyLen = nKeyLen;
	}
}
