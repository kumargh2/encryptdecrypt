package com.example.security;

public class NativeEncryptInput {
	public String nPlainText;
	public int nPlainTextLen;
	public String nIv;
	public int nIvLen;
	public String nAad;
	public int nAadLen;
	public String salt;
	public int saltLen; 
	public String password;
	public int passwdLen; 
	public int iterations; 
	public int nKeyLen; 
}
