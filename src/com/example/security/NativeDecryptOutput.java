package com.example.security;

public class NativeDecryptOutput {
	public String nPlainText;
	public int nPlainTextLen;
	NativeDecryptOutput(String nPlainText, int nPlainTextLen) {
		this.nPlainText = nPlainText;
		this.nPlainTextLen = nPlainTextLen;
	}
}
