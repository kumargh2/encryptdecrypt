##!/bin/bash
# file.sh: a sample shell script to demonstrate the concept of Bash shell functions
# define usage function

cd ~/workspace/openssl-android/jni
ndk-build NDK-DEBUG=1
cp ../libcrypto.so ../libs/armeabi-v7a/
cd ..
ant debug
adb uninstall com.example.security
adb install /home/kumar/workspace/openssl-android/bin/myopenssl-debug.apk
