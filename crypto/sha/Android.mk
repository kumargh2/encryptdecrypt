LOCAL_PATH:= $(call my-dir)

#arm_cflags := -DOPENSSL_BN_ASM_MONT -DAES_ASM -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM
#arm_src_files := \
#    asm/sha1-armv4-large.s \
#    asm/sha256-armv4.s \
#    asm/sha512-armv4.s
#APP_ABI := armeabi

local_src_files := \
	sha1_one.c \
	sha1dgst.c \
	sha256.c \
	sha512.c \
	sha_dgst.c
  
local_c_includes := \
	$(NDK_PROJECT_PATH) \
	$(NDK_PROJECT_PATH)/crypto \
	$(NDK_PROJECT_PATH)/crypto/asn1 \
	$(NDK_PROJECT_PATH)/crypto/evp \
	$(NDK_PROJECT_PATH)/crypto/modes \
	$(NDK_PROJECT_PATH)/include \
	$(NDK_PROJECT_PATH)/include/openssl

local_c_flags := -DNO_WINDOWS_BRAINDEATH

# target
include $(CLEAR_VARS)
include $(LOCAL_PATH)/../../android-config.mk
LOCAL_SRC_FILES += $(local_src_files)
LOCAL_CFLAGS += $(local_c_flags)
LOCAL_C_INCLUDES += $(local_c_includes)
LOCAL_LDLIBS += -lz
LOCAL_CFLAGS += $(arm_cflags)
LOCAL_SRC_FILES += $(arm_src_files)
LOCAL_SRC_FILES += $(non_arm_src_files)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libsha
export CC="arm-linux-androideabi-gcc"
include $(BUILD_SHARED_LIBRARY)
	
